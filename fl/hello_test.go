package main

import (
	"testing"
)

func TestHello(t *testing.T) {
	hello := hello()
	if hello != "Hello, GitLab merge requests!" {
		t.Errorf("hello() = %s; want Hello, GitLab merge requests!", hello)
	}
}
